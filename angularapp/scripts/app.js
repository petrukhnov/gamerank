/**
 * Main config for application.
 */
angular.module('gamerank', [
  'ui.router',
  'ui.bootstrap',
  'restangular'
])
  .config(['$urlRouterProvider', function ($urlRouterProvider) {
    'use strict';
    //$urlRouterProvider.when('/', '/list');
    //$urlRouterProvider.otherwise('/');
  }])
  .value('baseUrl', location.protocol + '//' + location.hostname + (location.port && ':' + location.port) + location.pathname.replace(/[^\/]*$/, '') + 'gamerank/')
  .run(['$rootScope',
    function ($rootScope) {
      'use strict';

    }])
  .controller('GameRatingController', ['$scope', 'Restangular', function($scope, Restangular) {
    'use strict';

    var path = 'gamerank';

    $scope.maxstars = 10;
    $scope.curgame = null;

    $scope.games = [];

    var baseGames = Restangular.all(path+'/games');

    var restGetAll = function() {
      baseGames.getList().then(function (games) {
        $scope.games = games;
      });
    };
    restGetAll();


    $scope.hoveringOver = function(value) {
      $scope.overStar = value;
      $scope.percent = 100 * (value / $scope.maxstars);
    };

    $scope.clickSelectGame = function(game) {
      $scope.curgame = Restangular.copy(game);
    };

    $scope.clickDeleteGame = function(game) {
      game.remove().then(restGetAll);
      $scope.curgame = null;
    };

    $scope.clickSaveGame = function() {
      //save and refresh list
      if($scope.curgame.id>=0) {
        $scope.curgame.save().then(function (val) {
          $scope.curgame = val;
          restGetAll();
        });
      } else {
        baseGames.post($scope.curgame).then(function (val) {
          $scope.curgame = val;
          restGetAll();
        });
      }


    };

    $scope.clickAddGame = function() {
      $scope.curgame = {id: -1, name: '', rating: 0};
    };

  }]);


// Fix to allow html to js conversion of templates during build
angular.module('templates-main', []);
