'use strict';

var walk;

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  grunt.util.linefeed = '\n';

  // Define the configuration for all the tasks
  grunt.initConfig({

      // Project settings
      project     : {
        // configurable paths
        webapp: 'angularapp',
        test  : 'test/javascript',
        tmp   : '.tmp',
        dist  : 'dist',
        serverContext: '/gamerank'

      },

      // Watches files for changes and runs tasks based on the changed files
      watch       : {

        js        : {
          files  : ['<%= project.webapp %>/scripts/**/*.js'],
          tasks  : ['sails-linker', 'newer:jshint:all'],
          options: {
            livereload: true
          }
        },
        styles    : {
          files  : ['<%= project.webapp %>/styles/*.css'],
          tasks  : [],
          options: {
            livereload: true
          }
        },
//        sass      : {
//          files  : ['<%= project.webapp %>/sass/**/*.scss'],
//          tasks  : ['sass:dev'],
//          options: {
//            livereload: false
//          }
//        },
        less: {
          files: ['<%= project.webapp %>/less/**/*.less'],
          tasks: ['less:dev'],
          options: {
            livereload: false
          }
        },

        livereload: {
          files  : [
            '<%= project.webapp %>/**/{,*/}*.html',
            '<%= project.webapp %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
          ],
          options: {
            livereload: true
          }
        }
      },

      // The actual grunt server settings
      connect     : {
        options   : {
          port      : 9000,
          // Change this to '0.0.0.0' to access the server from outside.
          hostname  : 'localhost',
          livereload: 35729
        },
        proxies   : [
          {
            context: '<%= project.serverContext%>', // the context of the data service
            host   : 'localhost', // wherever the data service is running
            port   : 8080 // the port that the data service is running on
          }
        ],
        noLivereload: {
          options: {
            livereload: false,
            port      : 9000,
            base      : [
              '<%= project.tmp %>',
              '<%= project.webapp %>'
            ],
            middleware: function (connect, options) {
              var middlewares = [];

              if (!Array.isArray(options.base)) {
                options.base = [options.base];
              }

              // Setup the proxy
              middlewares.push(require('grunt-connect-proxy/lib/utils').proxyRequest);

              // Serve static files
              options.base.forEach(function (base) {
                middlewares.push(connect.static(base));
              });

              return middlewares;
            }
          }
        },
        livereload: {
          options: {
            open      : false,
            base      : [
              '<%= project.tmp %>',
              '<%= project.webapp  %>'
            ],
            middleware: function (connect, options) {
              var middlewares = [];

              if (!Array.isArray(options.base)) {
                options.base = [options.base];
              }

              // Setup the proxy
              middlewares.push(require('grunt-connect-proxy/lib/utils').proxyRequest);

              // Serve static files
              options.base.forEach(function (base) {
                middlewares.push(connect.static(base));
              });

              return middlewares;
            }
          }
        },
        test      : {
          options: {
            livereload: false,
            port      : 9001,
            base      : [
              '<%= project.tmp %>',
              '<%= project.webapp %>'
            ],
            middleware: function (connect, options) {
              var middlewares = [];

              if (!Array.isArray(options.base)) {
                options.base = [options.base];
              }

              // Setup the proxy
              middlewares.push(require('grunt-connect-proxy/lib/utils').proxyRequest);

              // Serve static files
              options.base.forEach(function (base) {
                middlewares.push(connect.static(base));
              });

              return middlewares;
            }
          }
        },
        dist      : {
          options: {
            open      : false,
            base      : '<%= project.dist %>',
            middleware: function (connect, options) {
              var middlewares = [];

              if (!Array.isArray(options.base)) {
                options.base = [options.base];
              }

              // Setup the proxy
              middlewares.push(require('grunt-connect-proxy/lib/utils').proxyRequest);

              // Serve static files
              options.base.forEach(function (base) {
                middlewares.push(connect.static(base));
              });

              return middlewares;
            }
          }
        }
      },

      // Make sure code css are up to par and there are no obvious mistakes
      jshint      : {
        options: {
          jshintrc: '.jshintrc',
          reporter: require('jshint-stylish')
        },
        all    : [
          'Gruntfile.js',
          '<%= project.webapp %>/scripts/**/*.js'
        ],
        test   : {
          options: {
            jshintrc: 'test/.jshintrc'
          },
          src    : ['test/spec/**/*.js']
        }
      },

      // Empties folders to start fresh
      clean       : {
        dist  : {
          files: [
            {
              dot: true,
              src: [
                '<%= project.tmp %>',
                '<%= project.dist %>/*'
              ]
            }
          ]
        },
        server: '<%= project.tmp %>'
      },

      // Add vendor prefixed css
      autoprefixer: {
        options: {
          browsers: ['last 1 version']
        },
        dist   : {
          files: [
            {
              expand: true,
              cwd   : '<%= project.tmp %>/styles/',
              src   : '*.css',
              dest  : '<%= project.tmp %>/styles/'
            }
          ]
        }
      },


      concat: {
        templates: {
          src : ['<%= project.tmp %>/concat/scripts/scripts.js', '<%= project.tmp %>/templates.js'],
          dest: '<%= project.tmp %>/concat/scripts/scripts.js'
        }
      },

      file_append   : {
        default_options: {
          files: {
            '<%= project.tmp %>/concat/scripts/scripts.js': {
              prepend: '<%= project.tmp %>/templates.js',
              input  : '<%= project.tmp %>/concat/scripts/scripts.js'
            }
          }
        }
      },


      // Automatically inject Bower components into the app
      'bowerInstall': {
        app : {
          src       : '<%= project.webapp %>/index.html',
          ignorePath: '<%= project.webapp %>/'
        },
        test: {
          src      : ['<%= project.test %>/karma-shared.conf.js'],
          fileTypes: {
            js: {
              block  : /(([\s\t]*)\/\/\s*bower:*(\S*))(\n|\r|.)*?(\/\/\s*endbower)/gi,
              detect : {
                js: /'.*\.js'/gi
              },
              replace: {
                js: '\'{{filePath}}\','
              }
            }
          }
        }
      },

      // Renames files for browser caching purposes
      rev           : {
        dist: {
          files: {
            src: [
              '<%= project.tmp %>/scripts/**/*.js',
              '<%= project.tmp %>/styles/**/*.css'
              //disabled until included in carat build (broke relative paths)
              //'<%= project.tmp %>/bower_components/**/*.{png,jpg,jpeg,gif,webp,svg,eot,ttf,woff}'
            ]
          }
        }
      },

      /**
       * HTML2JS is a Grunt plugin that takes all of your template files and
       * places them into JavaScript files as strings that are added to
       * AngularJS's template cache. This means that the templates too become
       * part of the initial payload as one JavaScript file. Neat!
       */
      html2js      : {
        options: {
          base: 'src/main/webapp'
          // custom options, see below
        },
        main   : {
          src : ['<%= project.webapp %>/scripts/**/*.html'],
          dest: '<%= project.tmp %>/templates.js'
        },
        test   : {
          src : ['<%= project.webapp %>/scripts/**/*.html'],
          dest: '<%= project.test %>/templates.js'
        }
      },


      // Reads HTML for usemin blocks to enable smart builds that automatically
      // concat, minify and revision files. Creates configurations in memory so
      // additional tasks can operate on them
      useminPrepare: {
        html   : '<%= project.webapp %>/index.html',
        options: {
          dest: '<%= project.tmp %>',
          flow: {
            html: {
              steps: {
                js : ['concat', 'uglifyjs'],
                css: ['cssmin']
              },
              post : {}
            }
          }
        }
      },

      // Performs rewrites based on rev and the useminPrepare configuration
      usemin       : {
        html   : ['<%= project.tmp %>/**/*.html'],
        css    : ['<%= project.tmp %>/styles/**/*.css'],
        options: {
          root      : ['<%= project.tmp %>'],
          assetsDirs: ['<%= project.tmp %>', '<%= project.tmp %>/bower_components'],
          patterns  : {
            // Dirty repalce of bower component assets, because usemin doesn't work as it should
            css: [
              [/(\/bower_components\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the bower references',
                function (value) {
                  grunt.log.write('HIT: ' + value);
                  return value.replace(/\/bower_components/g, '../bower_components');
                }],
              [/(\/images\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the image references',
                function (value) {
                  grunt.log.write('HIT: ' + value);
                  return value.replace(/\/images/g, '../images');
                }]
            ]
          }
        }
      },

      cssmin  : {
        options: {
          root: '<%= project.tmp %>/'
        },
        dist   : {
          expand: true,
          cwd   : '<%= project.tmp %>/',
          src   : ['*.css'],
          dest  : '<%= project.tmp %>/styles/',
          ext   : '.css'
        }
      },

      // The following *-min tasks produce minified files in the dist folder
      imagemin: {
        dist: {
          files: [
            {
              expand: true,
              cwd   : '<%= project.webapp %>/images',
              src   : '{,*/}*.{png,jpg,jpeg,gif}',
              dest  : '<%= project.dist %>/images'
            }
          ]
        }
      },
      svgmin  : {
        dist: {
          files: [
            {
              expand: true,
              cwd   : '<%= project.webapp %>/images',
              src   : '{,*/}*.svg',
              dest  : '<%= project.dist %>/images'
            }
          ]
        }
      },
      htmlmin : {
        dist: {
          options: {
            collapseWhitespace       : true,
            collapseBooleanAttributes: true,
            removeCommentsFromCDATA  : true,
            removeOptionalTags       : true
          },
          files  : [
            {
              expand: true,
              cwd   : '<%= project.tmp %>',
              src   : ['*.html'],
              dest  : '<%= project.tmp %>'
            }
          ]
        }
      },

      // Replace Google CDN references
      cdnify  : {
        dist: {
          html: ['<%= project.dist %>/*.html']
        }
      },

      uglify        : {
        options: {
          //disable variable substitution (todo fix it before release)
          mangle: false
        }
      },

      // Copies remaining files to places other tasks can use
      copy          : {
        tmp   : {
          files: [
            {
              expand: true,
              dot   : true,
              cwd   : '<%= project.webapp %>',
              dest  : '<%= project.tmp %>',
              src   : [
                '*.{ico,png,txt}',
                '.htaccess',
                '*.html',
                'bower_components/**/*',
                'libs/**/*',
                'images/**/*',
                'fonts/*',
                'styles/*',
                '**/*.json'
              ]
            },
            {
              expand: true,
              cwd   : '<%= project.webapp %>/images',
              dest  : '<%= project.tmp %>/images',
              src   : ['generated/*']
            }
          ]
        },
        dist  : {
          files: [
            {
              expand: true,
              dot   : true,
              cwd   : '<%= project.tmp %>',
              dest  : '<%= project.dist %>',
              src   : [
                '*.{ico,png,txt}',
                '.htaccess',
                '*.html',
                'images/**/*',
                'fonts/*',
                'scripts/*',
                'styles/*',
                '**/*.json'
              ]
            },
            {
              expand: true,
              cwd   : '<%= project.tmp %>',
              dest  : '<%= project.dist %>',
              src   : [
                'bower_components/**/*.{css,png,jpg,jpeg,gif,eebp,svg,eot,ttf,woff}'
              ]
            }
          ]
        },
        bower : {
          expand: true,
          cwd   : '<%= project.webapp %>',
          dest  : '<%= project.tmp %>',
          src   : [
            'bower_components/**/*.{png,jpg,jpeg,gif,eebp,svg,eot,ttf,woff}'
          ]
        },
        styles: {
          expand: true,
          cwd   : '<%= project.webapp %>',
          dest  : '.tmp/',
          src   : [
            'styles/{,*/}*.css',
            'bower_components/**/*.{css, png,jpg,jpeg,gif,webp,svg,eot,ttf,woff}'
          ]
        }

      },

      // Test settings
      karma         : {
        e2eOnce : {
          configFile: 'test/javascript/karma-e2e.conf.js',
          singleRun : true
        },
        unitOnce: {
          configFile: 'test/javascript/karma.conf.js',
          singleRun : true
        },
        unit    : {
          configFile: 'test/javascript/karma.conf.js',
          background: true
        }
      },

      //add js files to index
      'sails-linker': {
        defaultOptions: {
          options: {
            startTag: '<!--SCRIPTS-->',
            endTag  : '<!--SCRIPTS END-->',
            fileTmpl: '<script src="%s"></script>',
            appRoot : '<%= project.webapp %>/'
          },
          files  : {
            // Target-specific file lists and/or options go here.
            '<%= project.webapp %>/index.html': ['<%= project.webapp %>/scripts/**/*.js']
          }
        }
      },

      // Sass compilation
//      sass          : {                                 // task
//        dev: {                             // target
//          files: {                        // dictionary of files
//            '<%= project.webapp %>/styles/main.css': '<%= project.webapp %>/sass/main.scss'     // 'destination': 'source'
//          }
//        }
//      },
      less: {
        dev: {
          options: {
            paths: ['<%= project.webapp %>/less']
          },
          files: {
            '<%= project.webapp %>/styles/main.css': '<%= project.webapp %>/less/*.less'
          }
        },

        production: {
          options: {
            paths: ['<%= project.webapp %>/less'],
            cleancss: true,
            compress: true
          },
          files: {
            '<%= project.dist %>/css/main.css': '<%= project.webapp %>/less/*.less'
          }
        }
      },

      ngAnnotate: {
        options: {
          singleQuotes: true
        },
        all    : {
          files: {
            '<%= project.tmp %>/scripts/scripts.js': ['<%= project.tmp %>/concat/scripts/scripts.js']
          }
        }
      },


      protractor_webdriver: {
        options: {
          // Task-specific options go here.
        },
        run    : {
          // Target-specific file lists and/or options go here.
        }
      },

      protractor: {
        options: {
          configFile: 'node_modules/protractor/referenceConf.js', // Default config file
          keepAlive : false, // If false, the grunt process stops when the test fails.
          noColor   : false, // If true, protractor will not use colors in its output.
          args      : {
            // Arguments passed to the command
          }
        },
        test   : {
          options: {
            configFile: '<%= project.test %>/protractor-e2e.conf.js', // Target-specific config file
            debug     : false,
            args      : (function () {
              /**
               *      If the test:e2e task is called with '--only=something' we loop trough the files in the e2e
               *      spec folder and do a regex match with the 'only' parameter against the file name.
               *
               *      This allows the running of individual specs
               */

              var only = grunt.option('only');
              if (typeof only === 'undefined' || only === null) {
                return {};
              }
              var specs = [
                'test/javascript/e2e/specs/all.spec.js',
                'test/javascript/e2e/specs/home.spec.js'
              ];

              var files = walk('test/javascript/e2e/specs');

              files.forEach(function (file) {

                if (file.match(new RegExp(only, 'g'))) {
                  if (specs.indexOf(file) === -1) {
                    specs.push(file);
                  }
                }
              });
              if (specs.length <= 2) {
                throw 'Tried to only get the \'' + only + '\' e2e spec, but no such spec was found.';
              }

              return {
                specs: specs
              };


            })()
            // Target-specific arguments
          }
        }
      }

    }
  );

  grunt.registerTask('serve', function (target) {


    if (target === 'dist') {
      return grunt.task.run(['configureProxies:server', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'bowerInstall:app',
      'less:dev',
      'sails-linker',
      'autoprefixer',
      'configureProxies:server',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('serve:noReload', function (/*target*/) {


    grunt.task.run([
      'clean:server',
      'bowerInstall:app',
      'less:dev',
      'sails-linker',
      'autoprefixer',
      'configureProxies:server',
      'connect:noLivereload:keepalive'
    ]);
  });

  grunt.registerTask('pre-test', [
    'clean:server',
    'bowerInstall:test',
    'html2js:test',
    'autoprefixer'
  ]);


  grunt.registerTask('test:unit', [
    'pre-test',
    'configureProxies:test',
    'connect:test',
    'karma:unitOnce'
  ]);

  grunt.registerTask('test:unit:c', [
    'pre-test',
    'configureProxies:test',
    'connect:test',
    'karma:unitOnce',
    'karma:unit',
    'watch:karma'
  ]);

  grunt.registerTask('test:e2e', [
    'pre-test',
    'configureProxies:test',
    'connect:test',
    'protractor_webdriver:run',
    'protractor:test'
  ]);


  grunt.registerTask('test', [
    'pre-test',
    'configureProxies:test',
    'connect:test',
    'karma:unitOnce',
    'protractor_webdriver:run',
    'protractor:test'
  ]);


  grunt.registerTask('build', [
    'test',
    'build:skipTest'
  ]);
  grunt.registerTask('build:skipTest', [
    'clean:dist',
    'bowerInstall:app',
    'less:dev',
    'html2js:main',
    'sails-linker',
    'copy:tmp',
    'useminPrepare',
    'concat:generated',
    'concat:templates',
    'autoprefixer:dist',
    'ngAnnotate',
    'cdnify',
    'cssmin',
    'uglify',
    'rev',
    'usemin',
    'htmlmin',
    'copy:dist'
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'bower',
    'test',
    'build'
  ]);

// Shortcuts
  grunt.registerTask('b', 'build');
  grunt.registerTask('bst', 'build:skipTest');
  grunt.registerTask('c', 'clean');
  grunt.registerTask('s', 'serve');
  grunt.registerTask('sd', 'serve:dist');
  grunt.registerTask('t', 'test');
}
;

var fs = require('fs');

/**
 *
 *  Walks trough a directory and returns the files in that directory.
 *
 *  Used to regex filter specs for test:e2e
 */
walk = function (dir) {
  var results = [];
  var list = fs.readdirSync(dir);
  list.forEach(function (file) {
    file = dir + '/' + file;
    var stat = fs.statSync(file);
    if (stat && stat.isDirectory()) {
      results = results.concat(walk(file));
    } else {
      results.push(file);
    }
  });
  return results;
};
