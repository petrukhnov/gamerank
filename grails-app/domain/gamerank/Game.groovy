package gamerank

import groovy.transform.ToString
import groovy.transform.EqualsAndHashCode

@ToString(includeNames = true, includeFields = true, excludes = 'dateCreated,lastUpdated,metaClass')
@EqualsAndHashCode
class Game {

    Long id
    Long version
    String name
    Integer rating //0-10

    Date    dateCreated
    Date    lastUpdated

    static constraints = {
    }
}
