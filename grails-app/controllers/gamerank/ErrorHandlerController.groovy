package gamerank

import grails.converters.JSON
import org.springframework.http.HttpStatus

class ErrorHandlerController {

    def handle() {
        withFormat {
            json {
                response.setContentType "application/json; charset=utf-8"
                response.status = HttpStatus.INTERNAL_SERVER_ERROR
                render ["Move along, nothing to see here.", "${request.exception}"] as JSON
            }
        }
    }
}
