import gamerank.Game

class BootStrap {

    def init = { servletContext ->

        if (!Game.count()) {
            new Game(name: "Half Life", rating: 9).save(failOnError: true)
            new Game(name: "Quake", rating: 7).save(failOnError: true)
            new Game(name: "Doom", rating: 3).save(failOnError: true)
        }
    }
    def destroy = {
    }
}
