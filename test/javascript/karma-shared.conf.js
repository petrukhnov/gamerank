/**
 * Shared config file for Karma unit and e2e tests.
 *
 */
module.exports = function() {
  'use strict';

  var browsers = {
    chrome: {
      name: 'Chrome',
      launcher: 'karma-chrome-launcher'
    },
    phantomjs: {
      name: 'PhantomJS',
      launcher: 'karma-phantomjs-launcher'
    }
  };

  var browser = browsers.phantomjs;

  var bowerFiles = [
    // bower:js
    '../../angularapp/bower_components/jquery/dist/jquery.js',
    '../../angularapp/bower_components/angular/angular.js',
    '../../angularapp/bower_components/lodash/dist/lodash.compat.js',
    '../../angularapp/bower_components/bootstrap/dist/js/bootstrap.js',
    '../../angularapp/bower_components/angular-ui-router/release/angular-ui-router.js',
    '../../angularapp/bower_components/restangular/dist/restangular.js',
    // endbower
  ];


  bowerFiles = bowerFiles.map(function (file) {
    return file.replace('../../', '');
  });


  var testFiles = [
    'test/javascript/templates.js',
    'angularapp/scripts/*.js',
    'angularapp/scripts/**/*.js'

  ];


  testFiles = bowerFiles.concat(testFiles);

  return {
    // base path, that will be used to resolve files and exclude
    basePath: '../../',


    plugins: [
      browser.launcher,
      'karma-jasmine'
    ],

    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: testFiles,

    // list of files / patterns to exclude
    exclude: [],

    // web server port
    port : 9002,


    proxies: {
      '/images': 'http://localhost:9001/images'
    },


    reportSlowerThan: 200,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    reporters: ['dots'],


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [browser.name],


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  };
};
