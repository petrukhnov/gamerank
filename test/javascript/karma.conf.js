
/**
 *
 *     Configuration for Karma + Jasmine unit tests
 *
 *     Should be run trough a grunt task, which also launches a server at port 9001
 *     (see 'test' task), because image assets are proxied to that port.
 *
 *     If PhantomJs isn't working for you, you can change it by using another browser,
 *     but remember to also set the correct launcher plugin in the plugins
 *     array.
 *
 *     The grunt task bowerInstall:test is used to populate the 'bowerFiles' array
 *     with all the bower component files.
 *
 */


var sharedConfig = require('./karma-shared.conf.js');


module.exports = function(config) {
  'use strict';
  var conf = sharedConfig();


  conf.files = conf.files.concat([
    'test/javascript/mock/**/*.js',
    'test/javascript/spec/**/*.js'
  ]);

  conf.proxie = {
    '/images': 'http://localhost:9001/images'
  };

  config.set(conf);
};
